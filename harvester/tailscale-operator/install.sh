#!/usr/bin/env bash

set -euo pipefail

read -p "Tailscale OAuth Client ID: " OAUTH_ID
read -s -p "Tailscale OAuth Client secret: " OAUTH_SECRET

helm upgrade \
  --install \
  tailscale-operator \
  tailscale/tailscale-operator \
  --namespace=tailscale \
  --create-namespace \
  --set-string oauth.clientId="$OAUTH_ID" \
  --set-string oauth.clientSecret="$OAUTH_SECRET" \
  --set-string operatorConfig.hostname="harvester-operator" \
  --set-string apiServerProxyConfig.mode="true" \
  --wait

kubectl apply -f ingress.yaml
kubectl apply -f rancher-ingress.yaml
kubectl apply -f longhorn-ingress.yaml
kubectl apply -f connector.yaml
