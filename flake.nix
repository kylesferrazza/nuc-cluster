{
  description = "nuc-cluster";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.veleroNixpkgs.url = "github:nixos/nixpkgs/1d9afb7df9b58e6a664955191bec7d2aecc5d6ad";

  outputs =
    {
      self,
      flake-utils,
      nixpkgs,
      veleroNixpkgs,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        veleroPkgs = import veleroNixpkgs {
          inherit system;
        };
      in
      {
        devShell = pkgs.mkShell {
          name = "nuc-cluster";
          buildInputs = (with pkgs; [
            ansible
            talosctl
            kubectl
            argocd
            kubernetes-helm
            yq-go
            jq
            kubevirt
            virt-viewer
            renovate
            k9s
          ]) ++ (with veleroPkgs; [
            velero
          ]);
        };
        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
