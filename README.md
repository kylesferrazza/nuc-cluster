State for my Intel NUC Kubernetes Cluster.

# Setup

```
kubectl create ns external-secrets
echo -ne '<gitlab_token>' > token
kubectl -n external-secrets create secret generic gitlab-secret --from-file=token

kubectl apply -k ./setup
kubectl apply -k ./setup
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
kubectl -n argocd port-forward svc/argocd-server 8000:80
```

# Sync Waves

